<?php


add_action('add_meta_boxes', 'wordpressthemecourse_register_slide_metabox');
function wordpressthemecourse_register_slide_metabox()
{
    add_meta_box(
        'wordpressthemecourse_slide_metabox_id',                 // Unique ID
        'Slide Options',      // Box title
        'wordpressthemecourse_slide_metabox',  // Content callback, must be of type callable
        'slide'                            // Post type
    );
}

function wordpressthemecourse_slide_metabox($post)
{
    ?>
    <table class="form-table">
        <tr>
            <td><label for="slide">Slide image url</label></td>
            <td><input type="text" class="regular-text" name="slide" value=""></td>
        </tr>
    </table>


    <?php
}

add_action('save_post', 'wordpressthemecourse_save_slide_meta');
function wordpressthemecourse_save_slide_meta($post_id)
{
    update_post_meta( $post_id, 'slide', $_POST['slide']);
}
