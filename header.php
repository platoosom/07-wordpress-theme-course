<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="<?php bloginfo('description');?>">
    <meta name="author" content="watcharamet">
    <meta name="format-detection" content="telephone=no">
    <title><?php bloginfo('name');?></title>
    
    <?php wp_head(); ?>
</head>
<body class="layout-weightloss shop-page">
<!--header-->
<header class="header">
    <div class="header-quickLinks js-header-quickLinks d-lg-none">
        <div class="quickLinks-top js-quickLinks-top"></div>
        <div class="js-quickLinks-wrap-m">
        </div>
    </div>
    <div class="header-topline header-topline--compact d-none d-lg-flex">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-auto d-flex align-items-center">
                    <div class="header-info"><i class="icon-placeholder2"></i><?php echo get_option('wordpressthemecourse_address'); ?></div>
                    <div class="header-phone"><i class="icon-telephone"></i><a href="tel:<?php echo get_option('wordpressthemecourse_mobile') ?>"><?php echo get_option('wordpressthemecourse_mobile') ?></a></div>
                    <div class="header-info"><i class="icon-black-envelope"></i><a href="mailto:info@dentco.net">info@dentco.net</a></div>
                </div>
                <div class="col-auto ml-auto d-flex align-items-center">
						<span class="header-social">
							<a href="#" class="hovicon"><i class="icon-facebook-logo-circle"></i></a>
							<a href="#" class="hovicon"><i class="icon-twitter-logo-circle"></i></a>
							<a href="#" class="hovicon"><i class="icon-google-plus-circle"></i></a>
						</span>
                </div>
            </div>
        </div>
    </div>
    <div class="header-content">
        <div class="container">
            <div class="row align-items-lg-center">
                <button class="navbar-toggler collapsed" data-toggle="collapse" data-target="#navbarNavDropdown">
                    <span class="icon-menu"></span>
                </button>
                <div class="col-lg-auto col-lg-2 d-flex align-items-lg-center">
                    <a href="index.html" class="header-logo"><img src="<?php bloginfo('template_url');?>/images/logo.png" alt="" class="img-fluid"></a>
                </div>
                <div class="col-lg ml-auto header-nav-wrap">
                    <div class="header-nav js-header-nav">
                        <nav class="navbar navbar-expand-lg btco-hover-menu">
                            <?php
                            wp_nav_menu([
                                'theme_location' => 'main-menu',
                                'container_class' => 'collapse navbar-collapse justify-content-center',
                                'container_id' => 'navbarNavDropdown',
                                'menu_class' => 'navbar-nav',
                            ]);
                            ?>
                        </nav>
                    </div>
                    <div class="header-search">
                        <form id="form-search" action="<?php echo home_url(); ?>" method="get" class="form-inline">
                            <i class="icon-search" onclick="javascript:document.getElementById('form-search').submit()"></i>
                            <input name="s" type="text" placeholder="Search">
                            <button type="submit"><i class="icon-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--//header-->

<?php if( !is_home()){ ?>
    <!--quick links-->
    <div class="quickLinks-wrap js-quickLinks-wrap-d d-none d-lg-flex">
        <div class="quickLinks js-quickLinks">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col">
                        <a href="#" class="link">
                            <i class="icon-placeholder"></i><span>Location</span></a>
                        <div class="link-drop p-0">
                            <div class="google-map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d40119.804311386426!2d-97.32055794896301!3d37.64364017354126!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87bae4ec254beb5f%3A0x410df48edd2f5ede!2sGraceMed%20Jardine%20Family%20Clinic!5e0!3m2!1sen!2sua!4v1579853082410!5m2!1sen!2sua"></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <a href="#" class="link">
                            <i class="icon-clock"></i><span>Working Time</span>
                        </a>
                        <div class="link-drop">
                            <h5 class="link-drop-title"><i class="icon-clock"></i>Working Time</h5>
                            <table class="row-table">
                                <tr>
                                    <td><i>Mon-Thu</i></td>
                                    <td>08:00 - 20:00</td>
                                </tr>
                                <tr>
                                    <td><i>Friday</i></td>
                                    <td> 07:00 - 22:00</td>
                                </tr>
                                <tr>
                                    <td><i>Saturday</i></td>
                                    <td>08:00 - 18:00</td>
                                </tr>
                                <tr>
                                    <td><i>Sunday</i></td>
                                    <td>Closed</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col">
                        <a href="#" class="link">
                            <i class="icon-pencil-writing"></i><span>Request Form</span>
                        </a>
                        <div class="link-drop">
                            <h5 class="link-drop-title"><i class="icon-pencil-writing"></i>Request Form</h5>
                            <form id="requestForm" method="post" novalidate>
                                <div class="successform">
                                    <p>Your message was sent successfully!</p>
                                </div>
                                <div class="errorform">
                                    <p>Something went wrong, try refreshing and submitting the form again.</p>
                                </div>
                                <div class="input-group">
                                        <span>
                                                <i class="icon-user"></i>
                                            </span>
                                    <input name="requestname" type="text" class="form-control" placeholder="Your Name"/>
                                </div>
                                <div class="row row-sm-space mt-1">
                                    <div class="col">
                                        <div class="input-group">
                                                <span>
                                                        <i class="icon-email2"></i>
                                                    </span>
                                            <input name="requestemail" type="text" class="form-control" placeholder="Your Email"/>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="input-group">
                                                <span>
                                                        <i class="icon-smartphone"></i>
                                                    </span>
                                            <input name="requestphone" type="text" class="form-control" placeholder="Your Phone"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="selectWrapper input-group mt-1">
                                        <span>
                                                <i class="icon-micro"></i>
                                            </span>
                                    <select name="requestservice" class="form-control">
                                        <option selected="selected" disabled="disabled">Select Service</option>
                                        <option value="Molecular Testing & Oncology">Molecular Testing & Oncology</option>
                                        <option value="Histology">Histology</option>
                                        <option value="General Diagnostic Tests">General Diagnostic Tests</option>
                                        <option value="Naturopathic">Naturopathic</option>
                                        <option value="Genetics Tests">Genetics Tests</option>
                                        <option value="Cytology">Cytology</option>
                                    </select>
                                </div>
                                <div class="row row-sm-space mt-1">
                                    <div class="col-sm-6">
                                        <div class="input-group flex-nowrap">
                                                <span>
                                                            <i class="icon-calendar2"></i>
                                                        </span>
                                            <div class="datepicker-wrap">
                                                <input name="requestdate" type="text" class="form-control datetimepicker" placeholder="Date" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 mt-1 mt-sm-0">
                                        <div class="input-group flex-nowrap">
                                                <span>
                                                                <i class="icon-clock"></i>
                                                        </span>
                                            <div class="datepicker-wrap">
                                                <input name="requesttime" type="text" class="form-control timepicker" placeholder="Time" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right mt-2">
                                    <button type="submit" class="btn btn-sm btn-hover-fill">Request</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col">
                        <a href="#" class="link">
                            <i class="icon-calendar"></i><span>Doctors Timetable</span>
                        </a>
                        <div class="link-drop">
                            <h5 class="link-drop-title"><i class="icon-calendar"></i>Doctors Timetable</h5>
                            <p>This simply works as a guide and helps you to connect with dentists of your choice. Please confirm the doctor’s availability before leaving your premises.</p>
                            <p class="text-right"><a href="schedule.html" class="btn btn-sm btn-hover-fill">Details</a></p>
                        </div>
                    </div>
                    <div class="col">
                        <a href="#" class="link">
                            <i class="icon-price-tag"></i><span>Calculator</span>
                        </a>
                        <div class="link-drop">
                            <h5 class="link-drop-title"><i class="icon-price-tag"></i>Quick Pricing</h5>
                            <table class="row-table">
                                <tr>
                                    <td>Initial Consultation</td>
                                    <td>$10</td>
                                </tr>
                                <tr>
                                    <td>Dental check-up</td>
                                    <td>$15</td>
                                </tr>
                                <tr>
                                    <td>Routine Exam (no xrays)</td>
                                    <td>$50</td>
                                </tr>
                                <tr>
                                    <td>Simple Removal of a tooth</td>
                                    <td>$122</td>
                                </tr>
                                <tr>
                                    <td>Teeth cleaning</td>
                                    <td>$50 - $75</td>
                                </tr>
                                <tr>
                                    <td>X-ray image</td>
                                    <td>$10</td>
                                </tr>
                            </table>
                            <div class="text-right btn-wrap mt-2">
                                <a href="index.html" class="btn btn-sm btn-fill"><i class="icon-right-arrow"></i><span>Calculator</span><i class="icon-right-arrow"></i></a>
                                <a href="prices.html" class="btn btn-sm btn-fill"><i class="icon-right-arrow"></i><span>Details</span><i class="icon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <a href="#" class="link">
                            <i class="icon-emergency-call"></i><span>Emergency Case</span></a>
                        <div class="link-drop">
                            <h5 class="link-drop-title"><i class="icon-emergency-call"></i>Emergency Case</h5>
                            <p>Emergency dental care may be needed if you have had a blow to the face, lost a filling, or cracked a tooth. </p>
                            <ul class="icn-list">
                                <li><i class="icon-telephone"></i><span class="phone">1-800-267-0000<br>1-800-267-0001</span>
                                </li>
                                <li><i class="icon-black-envelope"></i><a href="mailto:info@besthotel-email.com">info@besthotel-email.com</a></li>
                            </ul>
                            <p class="text-right mt-2"><a href="contact.html" class="btn btn-sm btn-hover-fill">Our Contacts</a></p>
                        </div>
                    </div>
                    <div class="col col-close"><a href="#" class="js-quickLinks-close"><i class="icon-top" data-toggle="tooltip" data-placement="top" title="Close panel"></i></a></div>
                </div>
            </div>
            <div class="quickLinks-open js-quickLinks-open"><span data-toggle="tooltip" data-placement="left" title="Open panel">+</span></div>
        </div>
    </div>
    <!--//quick links-->
<?php } ?>

<div class="page-content">