<?php

add_action('admin_init', 'wordpressthemecourse_register_address');
function wordpressthemecourse_register_address()
{

    register_setting('general', 'wordpressthemecourse_address');

    add_settings_field(
        'wordpressthemecourse_address',
        'Address',
        'wordpressthemecourse_address_control',
        'general',
        'default',
        array('label_for' => 'wordpressthemecourse_address', 'value' => get_option('wordpressthemecourse_address'),)
    );

}

function wordpressthemecourse_address_control($args)
{
    ?>
    <input type="text" name="wordpressthemecourse_address" value="<?php echo $args['value']; ?>"/>
    <?php
}

add_action('admin_init', 'wordpressthemecourse_register_mobile');
function wordpressthemecourse_register_mobile()
{

    register_setting('general', 'wordpressthemecourse_mobile');

    add_settings_field(
        'wordpressthemecourse_mobile',
        'Mobile',
        'wordpressthemecourse_moble_control',
        'general',
        'default',
        array('label_for' => 'wordpressthemecourse_mobile', 'value' => get_option('wordpressthemecourse_mobile'),)
    );

}

function wordpressthemecourse_moble_control($args)
{
    ?>
    <input type="text" name="wordpressthemecourse_mobile" value="<?php echo $args['value']; ?>"/>
    <?php
}


add_action('admin_init', 'wordpressthemecourse_register_contactemail');
function wordpressthemecourse_register_contactemail()
{

    register_setting('general', 'wordpressthemecourse_contactemail');

    add_settings_field(
        'wordpressthemecourse_contactemail',
        'Contact email',
        'wordpressthemecourse_contactemail_control',
        'general',
        'default',
        array('label_for' => 'wordpressthemecourse_contactemail', 'value' => get_option('wordpressthemecourse_contactemail'),)
    );

}

function wordpressthemecourse_contactemail_control($args)
{
    ?>
    <input type="text" name="wordpressthemecourse_contactemail" value="<?php echo $args['value']; ?>"/>
    <?php
}

add_action('admin_init', 'wordpressthemecourse_register_socialfacebook');
function wordpressthemecourse_register_socialfacebook()
{

    register_setting('general', 'wordpressthemecourse_socialfacebook');

    add_settings_field(
        'wordpressthemecourse_socialfacebook',
        'Facebook',
        'wordpressthemecourse_socialfacebook_control',
        'general',
        'default',
        array('label_for' => 'wordpressthemecourse_socialfacebook', 'value' => get_option('wordpressthemecourse_socialfacebook'),)
    );

}

function wordpressthemecourse_socialfacebook_control($args)
{
    ?>
    <input type="text" name="wordpressthemecourse_socialfacebook" value="<?php echo $args['value']; ?>"/>
    <?php
}


add_action('admin_init', 'wordpressthemecourse_register_socialtwitter');
function wordpressthemecourse_register_socialtwitter()
{

    register_setting('general', 'wordpressthemecourse_socialtwitter');

    add_settings_field(
        'wordpressthemecourse_socialtwitter',
        'Twitter',
        'wordpressthemecourse_socialtwitter_control',
        'general',
        'default',
        array('label_for' => 'wordpressthemecourse_socialtwitter', 'value' => get_option('wordpressthemecourse_socialtwitter'),)
    );

}

function wordpressthemecourse_socialtwitter_control($args)
{
    ?>
    <input type="text" name="wordpressthemecourse_socialtwitter" value="<?php echo $args['value']; ?>"/>
    <?php
}


add_action('admin_init', 'wordpressthemecourse_register_socialgplus');
function wordpressthemecourse_register_socialgplus()
{

    register_setting('general', 'wordpressthemecourse_socialgplus');

    add_settings_field(
        'wordpressthemecourse_socialgplus',
        'Google+',
        'wordpressthemecourse_socialgplus_control',
        'general',
        'default',
        array('label_for' => 'wordpressthemecourse_socialgplus', 'value' => get_option('wordpressthemecourse_socialgplus'),)
    );

}

function wordpressthemecourse_socialgplus_control($args)
{
    ?>
    <input type="text" name="wordpressthemecourse_socialgplus" value="<?php echo $args['value']; ?>"/>
    <?php
}


/**
 * Social IG
 */
add_action('admin_init', 'wordpressthemecourse_register_socialig');
function wordpressthemecourse_register_socialig()
{

    register_setting('general', 'wordpressthemecourse_socialig');

    add_settings_field(
        'wordpressthemecourse_socialig',
        'IG',
        'wordpressthemecourse_socialig_control',
        'general',
        'default',
        array('label_for' => 'wordpressthemecourse_socialig', 'value' => get_option('wordpressthemecourse_socialig'),)
    );

}

function wordpressthemecourse_socialig_control($args)
{
    ?>
    <input type="text" name="wordpressthemecourse_socialig" value="<?php echo $args['value']; ?>"/>
    <?php
}
