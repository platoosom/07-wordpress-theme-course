

submitHandler: function submitHandler(form) {
    $(form).ajaxSubmit({
        type: "POST",
        data:  {
            'action': 'save_request',
            'requestname': $('input[name=requestname]').val(),
            'requestemail': $('input[name=requestemail]').val(),
            'requestphone': $('input[name=requestphone]').val(),
            'requestservice': $('input[name=requestservice]').val(),
            'requestdate': $('input[name=requestdate]').val(),
            'requesttime': $('input[name=requesttime]').val(),
        },
        url: requestforms.ajaxurl,
        success: function success(data) {
            console.log(data);
            if(data.success == 'yes'){
                alert('Your data is saved.')
                $('.successform', $requestForm).fadeIn();
                //$requestForm.get(0).reset();
            }
        },
        error: function error() {
            $('.errorform', $requestForm).fadeIn();
        }
    });
}